class AddColumnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :avatar, :string
    add_column :users, :birthday, :date
    add_column :users, :city, :string
    add_column :users, :phone, :integer
    add_column :users, :status, :boolean
    add_column :users, :gender, :integer
    
  end
end
