class Offer < ActiveRecord::Base
	 belongs_to :user, counter_cache: true
	 has_many :offer_photos
	 accepts_nested_attributes_for :offer_photos
end
