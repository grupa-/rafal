class HomeController < ApplicationController
def new
    @shout = Shout.new
  end
  
  def create
@shouts = Shout.all.limit(10).order('id DESC')

    @shout = Shout.new(params[:shout])
    if @shout.save
      redirect_to @shout
    else
      render :index
    end
  end
  
  def index
    @shouts = Shout.all.limit(5).order('id DESC')
  end

end