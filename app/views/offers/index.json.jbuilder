json.array!(@offers) do |offer|
  json.extract! offer, :id, :title, :cost, :picture, :start_date, :end_date, :area, :rooms, :localization, :description
  json.url offer_url(offer, format: :json)
end
