json.array!(@offer_photos) do |offer_photo|
  json.extract! offer_photo, :id, :offer_id, :photo
  json.url offer_photo_url(offer_photo, format: :json)
end
